package ch.bbw.architecturerefcard04;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArchitectureRefCard04Application {

    public static void main(String[] args) {
        SpringApplication.run(ArchitectureRefCard04Application.class, args);
    }

}


# Architecture Ref. Card 04 


# Umgebungsvariable für Maximum File Size

spring.servlet.multipart.max-file-size=5MB
spring.servlet.multipart.max-request-size=5MB

You can set the limits in KB, MB, GB, etc.



